#include <iostream>
#include <fstream>
#include "RelocRecordData.h"

RelocRecordData::RelocRecordData( unsigned int ofset,  unsigned int sn, std::string typeReloc)
{
	this->offset = ofset;
	this->serialNo = sn;
	this->typeReloc = typeReloc;
	operation =' ';
}


void RelocRecordData::setOp(char op)
{
	operation = op;
}
//ios::out | ios::app 

void RelocRecordData::writeToFile()
{
 std::ofstream myfile;
  myfile.open (outputFile, std::ios::out | std::ios::app);
  myfile<< offset << "	"<<typeReloc << "	"<< serialNo <<"	"<<operation<<std::endl;
  myfile.close();
}


void RelocRecordData::writeToFile(int serialNumber)
{
 std::ofstream myfile;
  myfile.open (outputFile, std::ios::out | std::ios::app);
  myfile<< offset << "	"<<typeReloc << "	"<< serialNumber <<"	"<<operation<<std::endl;
  myfile.close();
}