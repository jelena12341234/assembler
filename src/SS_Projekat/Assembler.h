#pragma once

#include <string>
#include <unordered_map>
#include <regex>
#include <bitset>
#include "RelocRecordData.h"
#include "Section.h"
#include "flinkData.h"

class SymbolTable;
class Section;
class RelocRecordData;
class FlinkData;

extern char *outputFile;

class equInfo
{
public:
	std::string section;
	int serialNo; //if local then sNo of its section else its sNo
	char op;	  //- or +
	equInfo(std::string s, int sno, char o)
	{
		section = s;
		serialNo = sno;
		op = o;
	}
};

class equValue
{
public:
	std::string equName;
	char op;
	std::string label;
	equValue(std::string en, char o, std::string l)
	{
		equName = en;
		op = o;
		label = l;
	}
};

class Assembler
{
	char size1B, size2B;
	bool oneByte;
	static unsigned long int locationCounter;
	std::regex operations, symbol, literal, immLit, immSym, regdir, regdirL, regdirH, hexaNum, immHexaNum;
	std::regex regind, regind_pomLit, regind_pomSym, memLit, memSym, expressionSym, expressionLit;
	std::vector<std::vector<RelocRecordData>> relocs;
	std::vector<std::vector<Section>> sections;
	std::unordered_map<std::string, int> equSections;
	std::vector<std::unordered_map<std::string, char>> namesSections; //na kraju se proverava da li ima svih sekcija PAMTIS IMENA SIMBOLA
	std::bitset<8> opDescr;
	std::vector<std::vector<equInfo>> relocForEqu;

	std::vector<equValue> equBP; 	//backpatching for equ directives, once known we go through vector

public:
	static SymbolTable symbolTable;
	static SymbolTable finalSTable;

	Assembler();

	virtual ~Assembler();

	void checkIfDefined();

	std::vector<Section> getSection(std::string);

	void checkForReloc(std::string sec, unsigned long int lc);

	int ifRelocExists(std::vector<RelocRecordData>, unsigned long int lc);

	void setContent(int index, std::string, std::string content);

	void backPatching(std::vector<FlinkData> addresses, int value, std::string sect);

	void OnByteMatched(std::string symbols, unsigned int line);

	void OnWordMatched(std::string symbols, unsigned int line);

	void OnEquMatched(std::string symbols, unsigned int line);

	int addressing(std::string op);

	int jumpDetected(std::string adr);

	bool checkAddressing(std::bitset<8> bs);

	void writeIntoSection(std::string, unsigned long int i);

	void writeIntoRelRecord(RelocRecordData rrd);

	void onLabelMatched(std::string label, unsigned int line);

	void onEndMatched();

	void onSkipMatched(std::string value);

	void onExternMatched(std::string symbols, unsigned int line);

	void onGlobalMatched(std::string symbols, unsigned int line);

	void onSectionMatched(std::string section, unsigned int line);

	void OnDataMatched(std::string symbols, unsigned int line);

	void onAddDetected(std::string symbols, unsigned int line);

	void onPushDetected(std::string symbols, unsigned int line);

	void onPopDetected(std::string symbols, unsigned int line);

	void onXchgDetected(std::string symbols, unsigned int line);

	void onMovDetected(std::string symbols, unsigned int line);

	void onSubDetected(std::string symbols, unsigned int line);

	void onMulDetected(std::string symbols, unsigned int line);

	void onDivDetected(std::string symbols, unsigned int line);

	void onCmpDetected(std::string symbols, unsigned int line);

	void onNotDetected(std::string symbols, unsigned int line)
	{
		basicInstr(symbols, line, "not");
	}

	void onAndDetected(std::string symbols, unsigned int line)
	{
		basicInstr(symbols, line, "and");
	}

	void onOrDetected(std::string symbols, unsigned int line)
	{
		basicInstr(symbols, line, "or");
	}

	void onXorDetected(std::string symbols, unsigned int line)
	{
		basicInstr(symbols, line, "xor");
	}

	void onTestDetected(std::string symbols, unsigned int line)
	{
		basicInstr(symbols, line, "test");
	}

	void onShlDetected(std::string symbols, unsigned int line)
	{
		basicInstr(symbols, line, "shl");
	}

	void onShrDetected(std::string symbols, unsigned int line)
	{
		basicInstr(symbols, line, "shr");
	}

	void onJGTDetected(std::string symbols, unsigned int line);

	void onHaltDetected(std::string symbols, unsigned int line)
	{
		this->writeIntoSection("00", this->locationCounter++);
	}

	void onIRetDetected(std::string symbols, unsigned int line)
	{
		this->writeIntoSection("08", this->locationCounter++);
	}

	void onJNEDetected(std::string symbols, unsigned int line);

	void onJEQDetected(std::string symbols, unsigned int line);

	void onCallDetected(std::string symbols, unsigned int line);

	void onJMPDetected(std::string symbols, unsigned int line);

	void onRetDetected(std::string symbols, unsigned int line)
	{
		this->writeIntoSection("18", this->locationCounter++);
	}

	void onINTDetected(std::string symbols, unsigned int line)
	{
		this->writeIntoSection("18", this->locationCounter++);
	}

	void basicInstr(std::string symbols, unsigned int line, std::string nameOfInstr);

	void jump(std::string symbols, unsigned int line, std::string nameOfInstr);

	void addToSection(std::string section, char op);

	bool doesEquExists(std::string name)
	{
		if (equSections.find(name) == equSections.end())
			return false;
		else
			return true;
	}

	void addPadding(std::string &num)
	{
		if (num.size() == 3)
		{
			num = "0" + num;
			return;
		}
		if (num.size() < 2)
			num = "0" + num;
		if (num.size() == 3)
			num = num + "0";
		if (num.size() < 4)
			num = num + "00";
	}
};
