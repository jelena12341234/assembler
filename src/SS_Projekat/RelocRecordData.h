#pragma once
#include <string>

extern char *outputFile;

class RelocRecordData
{

	unsigned int offset;
	unsigned int serialNo;
	std::string typeReloc;
	std::string section;
	char operation;

public:
	RelocRecordData(unsigned int ofset, unsigned int sn, std::string typeReloc);

	RelocRecordData(unsigned int ofset, unsigned int sn, std::string typeReloc, std::string sec)
	{
		this->offset = ofset;
		this->serialNo = sn;
		this->typeReloc = typeReloc;
		operation = ' ';
		section = sec;
	}

	void setOp(char op);

	std::string getSection()
	{
		return section;
	}

	std::string getTypeReloc()
	{
		return typeReloc;
	}

	void writeToFile();

	void writeToFile(int serialNumber);

	unsigned int getSerialNo()
	{
		return serialNo;
	}

	void setSerialNo(unsigned int s)
	{
		serialNo = s;
	}

	unsigned int getOffset()
	{
		return offset;
	}
};
