#include "FlexLexer.h"
#include <fstream>
#include <string>
#include <iostream>


 char *outputFile;

int main(int argc, char **argv)
{
	//asembler -o ulaz1.o ulaz1.s
	if (argc != 4)
	{
		std::cout << "not enough parameters, correct form is: ./prog -o output input" << std::endl;
		exit(-1);
	}
	if (argv[1] == "-o")
	{
		std::cout << "invalid function" << argv[1]<<"*" << std::endl;
		exit(-1);
	}

	outputFile = argv[2];

	std::ifstream fez;
	std::string filename(argv[3]);
	fez.open(argv[3]);
	if(!fez.is_open()){
		std::cout<<"didnt open "<<argv[3]<<std::endl;
		exit(-1);
	}
	yyFlexLexer lekser(fez, std::cout);
	lekser.yylex();
	fez.close();
	return 0;

}
