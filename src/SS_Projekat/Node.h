#pragma once
#include <string>
#include <vector>

class FlinkData;

class Node {
public:
	bool defined;
	bool local; //true for local false for global
	int value;
	int size;
	unsigned int offset;
	unsigned int serialNum;
	std::string label;	//name
	std::string type;	//section, data, src file or func
	std::string section;
	std::vector<FlinkData> flink;

	Node(std::string l, std::string s, unsigned int os, bool flag);

	Node(std::string l, std::string s, unsigned int os, bool flag, bool def, int val);

	Node(std::string l, std::string s, unsigned int os, bool flag, bool def, int val, int sz);

	void setSerialNum(unsigned int);

	void addBPaddress(std::string adr, unsigned long int lc);

	std::string getSection() 
	{
		return section;
	}

	void setSection(std::string s)
	{
		section = s;
	}
};
