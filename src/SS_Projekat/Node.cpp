#include "Node.h"
#include "flinkData.h"

Node::Node(std::string l, std::string s, unsigned int os, bool flag)
{
	label = l;
	section = s;
	offset = os;
	local = flag;
}

Node::Node(std::string l, std::string s, unsigned int os, bool flag, bool def, int val)
{
	label = l;
	section = s;
	offset = os;
	local = flag;
	defined = def; 
	value = val;
	size = 0;
}


Node::Node(std::string l, std::string s, unsigned int os, bool flag, bool def, int val, int sz)
{
	label = l;
	section = s;
	offset = os;
	local = flag;
	defined = def; 
	value = val;
	size = sz;
}

void Node::setSerialNum(unsigned int id)
{
	this->serialNum = id;
}


void Node::addBPaddress(std::string sec ,unsigned long int lc){
	FlinkData fd(sec,lc);
	flink.push_back(fd);
}
