#pragma once
#include<string>

class Section{
    unsigned long int offset; //dec
    std::string content;  //sadrzaj u hexa 
    std::string section;
public:
    Section(unsigned long int of, std::string con){
        offset = of;
        content = con;
    }

    Section(unsigned long int of, std::string con, std::string sec){
        offset = of;
        content = con;
        section = sec;
    }

    std::string getSection()
    {
        return section;
    }

   // Section(const Section &s)
    //{
      //  offset = s.offset;
       // content = s.content;
    //}

    unsigned int getOffset()
    {
        return offset;
    }

    void setContent(std::string cnt)
    {
        content = cnt;
    }

    std::string getContent()
    {
        return content;
    }
};