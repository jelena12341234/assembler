#pragma once
#include <unordered_map> 
#include <string>
#include "Node.h"

extern char *outputFile;

class SymbolTable
{
	static unsigned long int ID;
	std::string currentSection;
public:

	std::unordered_map<std::string, Node> umap;

	void insert(Node node, std::string name);

	Node getNode(std::string name);
	
	SymbolTable()
	{
		currentSection = "UND";
	}

	void setSize(std::string name, int sz);

	void setCurrentSection(std::string sect);
	
	std::string getCurrentSection()
	{
		return this->currentSection;
	}

	void setValue(std::string name, int lc)
	{
		umap.at(name).value = lc;
	}

	void setSection(std::string name)
	{
		umap.at(name).section = this->currentSection;
	}

	bool doesExist(std::string nameOfLabel);

	unsigned int getSerialNo(std::string section);

	void checkIfDefined();

	void setDefined(std::string name){
		this->umap.at(name).defined = true;
	}
	
	void addBPaddress(std::string name, std::string sec ,unsigned long int lc);

	void removeFLinks(std::string section);
};

