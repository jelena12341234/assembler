#include <iostream>
#include "SymbolTable.h"
#include "Node.h"
#include "flinkData.h"
#include <fstream>

unsigned long int SymbolTable::ID = 0;

void SymbolTable::insert(Node node, std::string name)
{
	node.setSerialNum(SymbolTable::ID++);
	umap.insert(make_pair(name, node));
}

Node SymbolTable::getNode(std::string name)
{
	return umap.at(name);
}

void SymbolTable::setSize(std::string name, int sz)
{
	umap.at(name).size = sz;
}

void SymbolTable::addBPaddress(std::string name, std::string sec, unsigned long int lc)
{
	FlinkData fd(sec, lc);
	umap.at(name).flink.push_back(fd);
}

void SymbolTable::setCurrentSection(std::string sect)
{
	this->currentSection = sect;
}

bool SymbolTable::doesExist(std::string nameOfLabel)
{
	if (umap.find(nameOfLabel) == umap.end())
		return false;
	else
		return true;
}

unsigned int SymbolTable::getSerialNo(std::string section)
{
	return umap.at(section).serialNum;
}

/*
*/

void SymbolTable::checkIfDefined()
{
	for (auto it = umap.begin(); it != umap.end(); ++it)
	{
		if (!it->second.defined && it->second.local)
		{
			std::cout << "Symbol " << it->first << " not declared" << std::endl;
			return;
		}
	}
	std::ofstream myfile;
	myfile.open(outputFile);
	myfile << "<sym_table>" << std::endl;
	//#ime  sek  vr vel vid
	myfile << "#ime	sek	vr	vel	vid" << std::endl;

	for (int i = 1; i <= umap.size(); i++)
	{

		for (auto it = umap.begin(); it != umap.end(); ++it)
		{
			if (it->second.serialNum == i)
			{
				char c = it->second.local ? 'l' : 'g';
				if(it->second.section=="UND")
					myfile<< it->first << "	" << it->second.section << "	" << it->second.value << "	" << it->second.size << "	" << c << std::endl;
				else
				{
					myfile<< it->first << "	" << umap.at(it->second.section).serialNum << "	" << it->second.value << "	" << it->second.size << "	" << c << std::endl;
				}
				
			}
		}
	}

	myfile.close();
}

void SymbolTable::removeFLinks(std::string section)
{
	for (auto it = umap.begin(); it != umap.end(); ++it)
	{
		if (it->second.section == section)
			it->second.flink.resize(0);
	}
}