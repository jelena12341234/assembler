#include "Assembler.h"
#include <regex>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include "SymbolTable.h"
#include "Node.h"
#include "Section.h"
#include "flinkData.h"
#include "RelocRecordData.h"
#include <fstream>

SymbolTable Assembler::symbolTable;
SymbolTable Assembler::finalSTable;

unsigned long int Assembler::locationCounter = 0;

Assembler::Assembler()
{
	symbol = std::regex("[a-zA-Z]+");
	literal = std::regex("-?[1-9][0-9]*\\.?[0-9]*");
	regdir = std::regex("%r[1-8]");
	regind = std::regex("\\(%r[1-8]\\)");
	regind_pomLit = std::regex("[1-9][0-9]*\\(%r[1-8]\\)");
	regind_pomSym = std::regex("[a-zA-Z]+\\(%r[1-8]\\)");
	immLit = std::regex("\\$(-?[0-9][0-9]*\\.?[0-9]*)");
	immHexaNum = std::regex("\\$0x[0-9a-fA-F]+");
	immSym = std::regex("\\$[a-zA-Z]+");
	memLit = std::regex("[1-9][0-9]*");
	memSym = std::regex("[a-zA-Z]+");
	regdirL = std::regex("%r[1-8]l");
	regdirH = std::regex("%r[1-8]h");
	operations = std::regex("[-+]");
	expressionSym = std::regex("[a-zA-Z]+ | [1-9][0-9]*");
	expressionLit = std::regex("[\\+[1-9][0-9]* | -[1-9][0-9]*]");
	hexaNum = std::regex("0x[0-9a-fA-F]+");
	size1B = 0x00;
	size2B = 0x04;

	Node n = Node("UND", "UND", 0, true, true, 0, 0);
	this->symbolTable.insert(n, "UND");
}

Assembler::~Assembler()
{
}

int Assembler::addressing(std::string result)
{
	std::smatch m;
	std::smatch m1;

	if (std::regex_search(result, m, immHexaNum))
	{
		std::regex r1 = std::regex("(\\$)(.*)");
		if (std::regex_search(result, m1, r1))
		{
			this->oneByte = std::abs(std::stoi(m.str(1),nullptr,16)) <= 255 ? true : false;
			std::bitset<8> immBit(0x00);
			this->opDescr = immBit;
			return std::stoi(m.str(1));
		}
		else
		{
			std::cout << "No match is found immed lit" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, immLit))
	{
		std::regex r1 = std::regex("(\\$)(.*)");
		if (std::regex_search(result, m1, r1))
		{
			this->oneByte = std::abs(std::stoi(m.str(1))) <= 255 ? true : false;
			std::bitset<8> immBit(0x00);
			this->opDescr = immBit;
			return std::stoi(m.str(1));
		}
		else
		{
			std::cout << "No match is found immed lit" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, immSym))
	{
		std::regex r1 = std::regex("(\\$)(.*)");
		if (std::regex_search(result, m1, r1))
		{

			if (this->symbolTable.doesExist(m1.str(2)))
			{
				Node n1 = this->symbolTable.getNode(m1.str(2));
				this->opDescr = std::bitset<8>(0x00);
				unsigned int sn = n1.local ? this->symbolTable.getSerialNo(n1.section) : n1.serialNum;
				sn = n1.defined ? sn : n1.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);
				if (n1.defined)
				{
					this->oneByte = false; //this->oneByte = n1.value <= 255 ?  true : false;
				}
				else
				{
					this->oneByte = false;
					this->symbolTable.addBPaddress(m1.str(2), this->symbolTable.getCurrentSection(), this->locationCounter + 1);
				}
				return n1.value;
			}
			else
			{ //ako nije def
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(m1.str(2), this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter + 1);

				symbolTable.insert(n, m1.str(2));
				n.serialNum = this->symbolTable.getNode(m1.str(2)).serialNum;

				this->opDescr = std::bitset<8>(0x00);
				this->oneByte = false;
				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				sn = n.defined ? sn : n.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);
				return 0;
			}
		}
		else
		{
			std::cout << "No match is found immSym" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, regind_pomLit))
	{
		std::regex r1 = std::regex("(.*)(\\(%r)(.*)(\\))");
		if (std::regex_search(result, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(3)));
			opDescr = std::bitset<8>(std::string("01100000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			this->oneByte = false;
			return std::stoi(m1.str(2));
		}
		else
		{
			std::cout << "No match is found regind_pomLit" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, regind_pomSym))
	{
		std::regex r1 = std::regex("(.*)(\\(%r)(.*)(\\))");
		if (std::regex_search(result, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(3)));
			opDescr = std::bitset<8>(std::string("01100000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			this->oneByte = false;

			if (this->symbolTable.doesExist(m1.str(2)))
			{
				Node n1 = this->symbolTable.getNode(m1.str(2));
				unsigned int sn = n1.local ? this->symbolTable.getSerialNo(n1.section) : n1.serialNum;
				sn = n1.defined ? sn : n1.serialNum;
				if (std::stoi(m1.str(3)) == 7)
				{
					RelocRecordData rrd(this->locationCounter + 1, sn, "R_PC16", symbolTable.getCurrentSection());
					this->writeIntoRelRecord(rrd);
				}
				else
				{
					RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
					this->writeIntoRelRecord(rrd);
				}

				if (!n1.defined)
					this->symbolTable.addBPaddress(m1.str(2), this->symbolTable.getCurrentSection(), this->locationCounter + 1);

				return n1.value;
			}
			else
			{
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(m1.str(2), this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter + 1);
				
				symbolTable.insert(n, m1.str(2));
				n.serialNum = this->symbolTable.getNode(m1.str(2)).serialNum;

				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				sn = n.defined ? sn : n.serialNum;
				if (std::stoi(m1.str(5)) == 7)
				{
					RelocRecordData rrd(this->locationCounter + 1, sn, "R_PC16", symbolTable.getCurrentSection());
					this->writeIntoRelRecord(rrd);
				}
				else
				{
					RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
					this->writeIntoRelRecord(rrd);
				}
				return 0;
			}
		}
		else
		{
			std::cout << "No match is found regind_pomSym" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, regind))
	{
		std::regex r1 = std::regex("(\\(%r)(.*)(\\))");
		if (std::regex_search(result, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(2)));
			opDescr = std::bitset<8>(std::string("01000000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			oneByte = false;
			return -1;
		}
		else
		{
			std::cout << "No match is found regind" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, regdirL))
	{
		std::regex r1 = std::regex("(%r)(.*)(l)");
		if (std::regex_search(result, m1, r1))
		{
			std::bitset<8> regL(std::stoi(m1.str(2)));
			opDescr = std::bitset<8>(std::string("00100000"));
			opDescr[4] = regL[3];
			opDescr[3] = regL[2];
			opDescr[2] = regL[1];
			opDescr[1] = regL[0];
			oneByte = true;
			return -1;
		}
		else
		{
			std::cout << "No match is found regdirL" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, regdirH))
	{
		std::regex r1 = std::regex("(%r)(.*)(h)");
		if (std::regex_search(result, m1, r1))
		{
			std::bitset<8> regH(std::stoi(m1.str(2)));
			opDescr = std::bitset<8>(std::string("00100001"));
			opDescr[4] = regH[3];
			opDescr[3] = regH[2];
			opDescr[2] = regH[1];
			opDescr[1] = regH[0];
			oneByte = true;
			return -1;
		}
		else
		{
			std::cout << "No match is found regdirH" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, regdir))
	{
		std::regex r1 = std::regex("(%r)(.*)");
		if (std::regex_search(result, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(2)));
			opDescr = std::bitset<8>(std::string("00100000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			oneByte = false;
			return -1;
		}
		else
		{
			std::cout << "No match is found regdir" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, memLit))
	{
		std::regex r1 = std::regex("(.*)");
		if (std::regex_search(result, m1, r1))
		{
			this->oneByte = false;
			this->opDescr = std::bitset<8>(0x80);
			return std::stoi(m1.str(0));
		}
		else
		{
			std::cout << "No match is found memLit" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(result, m, memSym))
	{
		std::regex r1 = std::regex("(.*)");
		if (std::regex_search(result, m1, r1))
		{
			this->oneByte = false;
			this->opDescr = std::bitset<8>(0x80);

			if (this->symbolTable.doesExist(m1.str(0)))
			{
				Node n1 = this->symbolTable.getNode(m1.str(0));
				unsigned int sn = n1.local ? this->symbolTable.getSerialNo(n1.section) : n1.serialNum;
				sn = n1.defined ? sn : n1.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);

				if (!n1.defined)
					this->symbolTable.addBPaddress(m1.str(0), this->symbolTable.getCurrentSection(), this->locationCounter + 1);

				return n1.value;
			}
			else
			{
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(m1.str(0), this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter + 1);
				std::cout << "adresa za popravljanje je: " << this->locationCounter + 1 << std::endl;

				symbolTable.insert(n, m1.str(0));
				n.serialNum = this->symbolTable.getNode(m1.str(0)).serialNum;

				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				sn = n.defined ? sn : n.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);
				return 0;
			}
		}
		else
		{
			std::cout << "No match is found memSym" << std::endl;
			return -2;
		}
	}
	else
	{
		std::cout << "No match is found at all" << result << std::endl;
		return 0;
	}
}
int Assembler::jumpDetected(std::string adr)
{
	std::smatch m;
	std::smatch m1;

	if (std::regex_search(adr, m, std::regex("\\*[1-9][0-9]*\\(%r[1-8]\\)")))
	{ //for regind_pomLit
		std::regex r1 = std::regex("(\\*)(.*)(\\(%r)(.*)(\\))");
		if (std::regex_search(adr, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(4)));
			opDescr = std::bitset<8>(std::string("01100000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			return std::stoi(m1.str(2));
		}
		else
		{
			std::cout << "No match is found regIndPom Lit" << std::endl;
			return -3;
		}
	}
	else if (std::regex_search(adr, m, std::regex("\\*[a-zA-Z]+\\(%r[1-8]\\)")))
	{ //for regind_pomSym

		std::regex r1 = std::regex("(\\*)(.*)(\\(%r)(.*)(\\))");
		if (std::regex_search(adr, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(4)));
			opDescr = std::bitset<8>(std::string("01100000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			this->oneByte = false;

			if (this->symbolTable.doesExist(m1.str(2)))
			{
				Node n1 = this->symbolTable.getNode(m1.str(2));
				unsigned int sn = n1.local ? this->symbolTable.getSerialNo(n1.section) : n1.serialNum;
				sn = n1.defined ? sn : n1.serialNum;
				if (this->symbolTable.getCurrentSection() != n1.section) //&& n1.defined
					if (std::stoi(m1.str(4)) == 7)
					{
						RelocRecordData rrd(this->locationCounter + 1, sn, "R_PC16", symbolTable.getCurrentSection());
						this->writeIntoRelRecord(rrd);
					}
					else
					{
						RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
						this->writeIntoRelRecord(rrd);
					}

				if (!n1.defined)
				{
					this->symbolTable.addBPaddress(m1.str(2), this->symbolTable.getCurrentSection(), this->locationCounter + 1);
					std::cout << "simbol nije definisan, addBP: " << m1.str(2) << std::endl;
					return -2;
				}
				else if (n1.section == "UND")
					return -2;
				else
				{
					return n1.value;
				}
			}
			else
			{
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(m1.str(2), this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter + 1);
				std::cout << "adresa za popravljanje je: " << this->locationCounter + 1 << std::endl;

				symbolTable.insert(n, m1.str(2));
				n.serialNum = this->symbolTable.getNode(m1.str(2)).serialNum;

				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				sn = n.defined ? sn : n.serialNum;
				if (std::stoi(m1.str(4)) == 7)
				{
					RelocRecordData rrd(this->locationCounter + 1, sn, "R_PC16", symbolTable.getCurrentSection());
					this->writeIntoRelRecord(rrd);
				}
				else
				{
					RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
					this->writeIntoRelRecord(rrd);
				}
				return -2;
			}
		}
		else
		{
			std::cout << "No match is found regindpom sym" << std::endl;
			return -3;
		}
	}
	else if (std::regex_search(adr, m, std::regex("\\*[a-zA-Z]+")))
	{ //for memSym
		std::regex r1 = std::regex("(\\*)(.*)");
		if (std::regex_search(adr, m1, r1))
		{
			this->opDescr = std::bitset<8>(0x80);

			if (this->symbolTable.doesExist(m1.str(2)))
			{
				Node n1 = this->symbolTable.getNode(m1.str(2));
				unsigned int sn = n1.local ? this->symbolTable.getSerialNo(n1.section) : n1.serialNum;
				sn = n1.defined ? sn : n1.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);

				if (!n1.defined)
					this->symbolTable.addBPaddress(m1.str(2), this->symbolTable.getCurrentSection(), this->locationCounter + 1);

				return n1.value;
			}
			else
			{
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(m1.str(2), this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter + 1);
				std::cout << "adresa za popravljanje je: " << this->locationCounter + 1 << std::endl;

				symbolTable.insert(n, m1.str(2));
				n.serialNum = this->symbolTable.getNode(m1.str(2)).serialNum;

				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				sn = n.defined ? sn : n.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);
				return 0;
			}
		}
		else
		{
			std::cout << "No match is found sym" << std::endl;
			return -2;
		}
	}
	else if (std::regex_search(adr, m, std::regex("\\*[1-9][0-9]*")))
	{ //for memLit
		this->opDescr = std::bitset<8>(0x80);
		std::cout << "memLit";
	}
	else if (std::regex_search(adr, m, std::regex("\\*%r[1-8]")))
	{ //for  regdir
		std::regex r1 = std::regex("(\\*%r)(.*)");
		if (std::regex_search(adr, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(2)));
			opDescr = std::bitset<8>(std::string("00100000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			return -1;
		}
		else
		{
			std::cout << "No match is found immed" << std::endl;
			return -3;
		}
	}
	else if (std::regex_search(adr, m, std::regex("\\*\\(%r[1-8]\\)")))
	{ //for regind
		std::regex r1 = std::regex("(\\(\\*%r)(.*)(\\))");
		if (std::regex_search(adr, m1, r1))
		{
			std::bitset<8> reg(std::stoi(m1.str(2)));
			opDescr = std::bitset<8>(std::string("01000000"));
			opDescr[4] = reg[3];
			opDescr[3] = reg[2];
			opDescr[2] = reg[1];
			opDescr[1] = reg[0];
			return -1;
		}
		else
		{
			std::cout << "No match is found regind" << std::endl;
			return -3;
		}
	}
	else if (std::regex_search(adr, m, std::regex("[a-zA-Z]+")))
	{ //for Sym
		this->opDescr = std::bitset<8>(0x00);
		std::cout << "sym";

			if (this->symbolTable.doesExist(m.str(0)))
			{
				Node n1 = this->symbolTable.getNode(m.str(0));
				unsigned int sn = n1.local ? this->symbolTable.getSerialNo(n1.section) : n1.serialNum;
				sn = n1.defined ? sn : n1.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);

				if (!n1.defined)
					this->symbolTable.addBPaddress(m.str(0), this->symbolTable.getCurrentSection(), this->locationCounter + 1);

				if(!symbolTable.umap.at(m.str(0)).local) return 0;
				else return n1.value;
			}
			else
			{
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(m.str(0), this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter + 1);
				std::cout << "adresa za popravljanje je: " << this->locationCounter + 1 << std::endl;

				symbolTable.insert(n, m.str(0));
				n.serialNum = this->symbolTable.getNode(m.str(0)).serialNum;

				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				sn = n.defined ? sn : n.serialNum;
				RelocRecordData rrd(this->locationCounter + 1, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);
				return 0;
			}
		
	}
	else if (std::regex_search(adr, m, std::regex("[1-9][0-9]*")))
	{ //for Lit
		this->opDescr = std::bitset<8>(0x00);
		std::regex r1 = std::regex("(.*)");
		if (std::regex_search(adr, m1, r1))
		{
			return std::stoi(m1.str(1));
		}
		else
		{
			std::cout << "No match is found mem imm " << std::endl;
			return -3;
		}
	}
	else
	{
		std::cout << "No match is found at all" << std::endl;
		return 0;
	}

	return 0;
}

void Assembler::onLabelMatched(std::string label, unsigned int line)
{
	std::regex r("[A-Za-z_][A-Za-z_0-9]*");
	std::smatch m;
	std::string result;
	if (std::regex_search(label, m, r))
	{
		result = m.str(0);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}


	unsigned offset = this->locationCounter;
	bool local = true;
	Node n(result, this->symbolTable.getCurrentSection(), offset, local, true, this->locationCounter); //true for defined, 0 for val
	if (!symbolTable.doesExist(result))
		symbolTable.insert(n, result);
	else
	{ 
		
	//	std::string typeReloc="R_16";
		if (symbolTable.umap.at(result).local)
		{	
			for(int i=0; i < relocs.size(); i++)
				for(int j=0; j<relocs[i].size(); j++)
					if (relocs[i][j].getSerialNo() == symbolTable.umap.at(result).serialNum)	
						relocs[i][j].setSerialNo(this->symbolTable.umap.at(symbolTable.getCurrentSection()).serialNum);		
		} 
	/*	else 
		{
			for(int i=0; i < relocs.size(); i++)
				for(int j=0; j<relocs[i].size(); j++)
					if (relocs[i][j].getSerialNo() == symbolTable.umap.at(result).serialNum)	//ako je pcrel treba da nestane reloc
						typeReloc = relocs[i][j].getTypeReloc();
		}*/
		

		this->symbolTable.setSection(result);
		this->symbolTable.setDefined(result);
		this->symbolTable.setValue(result, this->locationCounter);
		std::vector<FlinkData> adresses = symbolTable.getNode(result).flink;
		int val = this->locationCounter;
		//ako su u istoj sekciji i ako je PC_REL treba raditi backPatching

	/*	if(typeReloc != "R_16")
		{
			this->backPatching(adresses, val, this->symbolTable.getCurrentSection());
			return;	
		}
	*/

		for(int i=0; i<equBP.size(); i++)
		{
			if(equBP[i].label == result && equBP[i].op == '-')
				symbolTable.umap.at(equBP[i].equName).value -= val; 
			else symbolTable.umap.at(equBP[i].equName).value += val; 
		}
		if (!symbolTable.getNode(result).local || this->symbolTable.getCurrentSection() == ".bss") 	return;
		this->backPatching(adresses, val, this->symbolTable.getCurrentSection());
	}
}

void Assembler::onEndMatched()
{
	std::cout << ".end" << std::endl;
	this->symbolTable.setSize(this->symbolTable.getCurrentSection(), this->locationCounter);
	this->checkIfDefined();
	std::ofstream myfile;
	myfile.open(outputFile, std::ios::out | std::ios::app);

	for(int i=0; i<sections.size(); i++)
	{
		if(sections[i].size()>0)
		{
			myfile << "<"<<sections[i][0].getSection()<<">:" << std::endl;
			for(int j=0; j < sections[i].size(); j++)
			myfile << sections[i][j].getContent() << std::endl;		
		}
	}
	myfile.close();
	//********dodatak*****
	for(int i=0; i < namesSections.size(); i++)
	{	
		std::unordered_map<std::string, int> currentEqu;
		for(auto it = namesSections[i].begin(); it != namesSections[i].end(); it++)
		{
			if (currentEqu.find(symbolTable.umap.at(it->first).section) == currentEqu.end())
			{	
				if(it->second == '+') currentEqu.insert(std::make_pair(symbolTable.umap.at(it->first).section,1));
				else currentEqu.insert(std::make_pair(symbolTable.umap.at(it->first).section,-1));
			}
			else
			{
				if(it->second == '+') currentEqu.at(symbolTable.umap.at(it->first).section)++;
				else currentEqu.at(symbolTable.umap.at(it->first).section)--;
			}	
		}

		int sum = 0;
		for(auto it = currentEqu.begin(); it != currentEqu.end(); it++)
			sum += it->second;
		
		if(sum > 1)
		{
			std::cout<<"GRESKA KOD EQUA, NEKA SEKCIJA JE >1"<<std::endl;
			exit(-1);
		}

	}

}

void Assembler::onSkipMatched(std::string value)
{
	std::regex r("(\\.skip[ \t\r\n]+)([1-9][0-9]*)");
	std::smatch m;
	std::string result;
	if (std::regex_search(value, m, r))
	{
		result = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	for (int i = 0; i < stoi(result); i++)
	{
		writeIntoSection("00", this->locationCounter++);
	}
}

void Assembler::onExternMatched(std::string symbols, unsigned int line)
{
	std::regex r("(\\.extern[ \t\r\n]+)([A-Za-z]+(,[ \t\r\n]+[A-Za-z]+)*)");
	std::smatch m;
	std::string result;
	if (std::regex_search(symbols, m, r))
	{
		result = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	std::stringstream inputstr(result);

	for (std::string word; std::getline(inputstr, word, ',');)
	{
		if (word[0] == ' ')
			word = word.substr(1, word.size());
		unsigned offset = this->locationCounter; //dont need it
		bool local = false;						 //global

		Node n(word, "UND", offset, local, true, 0); //fal

		symbolTable.insert(n, word);
	}
}
void Assembler::OnWordMatched(std::string symbols, unsigned int line)
{
	std::cout<<"wordMatched"<<std::endl;
	std::regex r("(\\.word[ \t\r\n]+)(.*(,[ \t\r\n]+.*)*)");
	std::smatch m;
	std::string result;
	if (std::regex_search(symbols, m, r))
	{
		result = m.str(2);
		std::cout << "line 553: " << result << std::endl;
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	std::stringstream inputstr(result);
	for (std::string word; std::getline(inputstr, word, ',');)
	{
		std::smatch mLit, mSym, mOp, mHex;
		if (word[0] == ' ')
			word = word.substr(1, word.size());

		std::string res;

		if(std::regex_search(word,mHex,hexaNum))
		{
			 int num = std::stoi(word, nullptr, 16);
			std::cout<<"HEXA NUM FOUND "<<num<<std::endl;
			std::stringstream stream1;
			stream1 << std::hex << num;
			std::string res1(stream1.str());
			addPadding(res1);
			this->writeIntoSection(res1, this->locationCounter);
			this->locationCounter += 2;
		}	
		else if (std::regex_search(word, mSym, symbol))
		{
			if (this->symbolTable.doesExist(word))
			{
				Node n1 = this->symbolTable.getNode(word);
				unsigned int sn = n1.local ? this->symbolTable.getSerialNo(n1.section) : n1.serialNum;
				RelocRecordData rrd(this->locationCounter , sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);

				if (!n1.defined)
					this->symbolTable.addBPaddress(word, this->symbolTable.getCurrentSection(), this->locationCounter + 1);

				std::stringstream stream1;
				stream1 << std::hex << n1.value;

				std::string res1(stream1.str());
				addPadding(res1);
				this->writeIntoSection(res1, this->locationCounter);
				this->locationCounter += 2;
			}
			else
			{ //ako nije def
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(word, this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter);
				symbolTable.insert(n, word);
				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				RelocRecordData rrd(this->locationCounter, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);
				this->writeIntoSection("0000", this->locationCounter);
				this->locationCounter += 2;
			}
		}
		else if (std::regex_search(word, mLit, literal))
		{
			std::stringstream stream1;
			stream1 << std::hex << std::stoi(word);
			std::string res1(stream1.str());
			addPadding(res1);
			this->writeIntoSection(res1, this->locationCounter);
			this->locationCounter += 2;
		}
		else
		{
			//u slucaju izraza
			std::cout << "No match is found" << std::endl;
			return;
		}
	}
}

void Assembler::OnByteMatched(std::string symbols, unsigned int line)
{

	std::regex r("(\\.byte[ \t\r\n]+)(.*(,[ \t\r\n]+.*)*)");
	std::smatch m;
	std::string result;
	if (std::regex_search(symbols, m, r))
	{
		result = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	std::stringstream inputstr(result);

	for (std::string word; std::getline(inputstr, word, ',');)
	{
		if (word[0] == ' ')
			word = word.substr(1, word.size());
		std::smatch mLit, mSym, mOp, mHex;
		std::string res;

		if(std::regex_search(word,mHex,hexaNum))
		{
			 int num = std::stoi(word, nullptr, 16);
			std::cout<<"HEXA NUM FOUND "<<num<<std::endl;
			std::stringstream stream1;
			stream1 << std::hex << num;
			if(num > 255) 
			{
				std::cout << "za byte vrednost upotrebljen broj koji ne moze stati u byte"<<std::endl;
				exit(-1);
			}
			std::string res1(stream1.str());
			if(res1.size() < 2) res1 = "0" + res1;
			this->writeIntoSection(res1, this->locationCounter);
			this->locationCounter += 2;
		}	else  if (std::regex_search(word, mLit, symbol))
		{
			Node n = this->symbolTable.getNode(word);
			if (n.defined)
			{
				std::stringstream stream;
				stream << std::hex << n.value;

				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				RelocRecordData rrd(this->locationCounter, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);

				std::string res1(stream.str());
				if(res1.size() < 2) res1 = "0" + res1;
				this->writeIntoSection(res1, this->locationCounter);
				this->locationCounter += 1;
			}
			else
			{ //ako nije def
				unsigned offset = this->locationCounter;
				bool local = true;
				Node n(word, this->symbolTable.getCurrentSection(), offset, local, false, 0);
				n.addBPaddress(this->symbolTable.getCurrentSection(), this->locationCounter);
				symbolTable.insert(n, word);
				unsigned int sn = n.local ? this->symbolTable.getSerialNo(n.section) : n.serialNum;
				RelocRecordData rrd(this->locationCounter, sn, "R_16", symbolTable.getCurrentSection());
				this->writeIntoRelRecord(rrd);
				this->writeIntoSection("00", this->locationCounter);
				this->locationCounter += 1;
			}
		}
		else if (std::regex_search(word, mLit, literal))
		{
			std::stringstream stream;
			stream << std::hex << std::stoi(word);
			std::string res1(stream.str());
			if(res1.size() < 2) res1 = "0" + res1;
			this->writeIntoSection(res1, this->locationCounter);
			this->locationCounter += 1;
		}
		else
		{
			//u slucaju izraza
			std::cout << "No match is found" << std::endl;
			return;
		}
	}
}

void Assembler::OnEquMatched(std::string symbols, unsigned int line)
{
	std::regex r("(\\.equ[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(2); //2 & 4 are operands
		src = m.str(4);	 //can be expression
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	std::smatch m1, mLit,mHex;
	char op = 'c';
	int lastValue = 0;
	
	if(!symbolTable.doesExist("bss"))
		{
			Node bss("bss","bss", 0, true, true, 0);
			symbolTable.insert(bss, "bss");
	}

	std::unordered_map<std::string, char> currentEqu;

	while (true)
	{


		std::stringstream inputstr(src);
		std::string word;
		std::getline(inputstr, word, '-');
		inputstr = std::stringstream(word);
		std::getline(inputstr, word, '+');
		std::cout << "WORD IS " << word << std::endl;

		if(std::regex_search(word,m1,hexaNum))
		{
			std::cout << "pronasao je " << m1.str(0);
			if(op == '-') lastValue-=std::stoi(m1.str(0),nullptr,16);
			else lastValue+=std::stoi(m1.str(0),nullptr,16);
			int len = m1.str(0).size();
			src = src.substr(len, src.size());
		}
		else if (std::regex_search(word, m1, symbol))
		{

			if(!symbolTable.doesExist(m1.str(0)))
			{
				Node n(m1.str(0), symbolTable.getCurrentSection(), 0, true, false, 0);
				symbolTable.insert(n,symbolTable.getCurrentSection());
				equBP.push_back(equValue(dest, op, m1.str(0)));
			}
			if (op != 'c')
				{this->addToSection(this->symbolTable.getNode(m1.str(0)).getSection(), op);
				 if (op=='-') lastValue-=symbolTable.umap.at(m1.str(0)).value;
				else lastValue+=symbolTable.umap.at(m1.str(0)).value;
				 }
			else
				{this->addToSection(this->symbolTable.getNode(m1.str(0)).getSection(), '+'); 
				 lastValue+=symbolTable.umap.at(m1.str(0)).value;
				}

			currentEqu.insert(std::make_pair(m1.str(0), op=='-'? '-':'+'));
				

			int len = m1.str(0).size();
			src = src.substr(len, src.size());
		}
		else if (std::regex_search(word, m1, literal))
		{
			if(op == '-') lastValue-=std::stoi(m1.str(0));
			else lastValue+=std::stoi(m1.str(0)); 
			int len = m1.str(0).size();
			src = src.substr(len, src.size());
		}

		if (src[0] == '-')
		{
			op = '-';
			src = src.substr(1, src.size());
		}
		else if (src[0] == '+')
		{
			op = '+';
			src = src.substr(1, src.size());
		}
		else break;
	}

	if(!symbolTable.doesExist(dest)){
	Node n(dest, "bss", 0, false, true, lastValue);
	}
	else 
	{
		symbolTable.umap.at(dest).value = lastValue;
		symbolTable.umap.at(dest).section = "bss";		
	}


	namesSections.push_back(currentEqu);
	
	//treba proveriti na kraju jer se mozda neki UND simbol definise => obrisi .clear()


	//samo jedan od ovih moze biti 1 svi ostali moraju biti nula
	/*
	RelocRecordData rrd(this->locationCounter , sn, "R_386_32" );
*/
}

void Assembler::onGlobalMatched(std::string symbols, unsigned int line)
{
	std::regex r("(\\.global[ \t\r\n]+)([A-Za-z]+(,[ \t\r\n]+[A-Za-z]+)*)");
	std::smatch m;
	std::string result;
	if (std::regex_search(symbols, m, r))
	{
		result = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	std::stringstream inputstr(result);

	for (std::string word; std::getline(inputstr, word, ',');)
	{
		if (word[0] == ' ')
			word = word.substr(1, word.size());

		unsigned offset = this->locationCounter; //will change later, depends on pos in currentSection & the length of instructions
		bool local = false;						 //global
		Node n(word, this->symbolTable.getCurrentSection(), offset, local, false, 0);

		symbolTable.insert(n, word);
	}
}

void Assembler::onSectionMatched(std::string section, unsigned int line)
{
	std::regex r("(\\.section[ \t\r\n]+)([A-Za-z_]+:)");
	std::smatch m;
	std::string result;
	if (std::regex_search(section, m, r))
	{
		result = m.str(2);
		result = result.substr(0, result.size() - 1);
		std::cout << result << std::endl;
		;
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	if (this->symbolTable.getCurrentSection() != "UND")
		this->symbolTable.setSize(this->symbolTable.getCurrentSection(), this->locationCounter);

	this->symbolTable.setCurrentSection(result);
	this->locationCounter = 0;

	unsigned offset = 0;
	bool flag = true;
	Node n(result, this->symbolTable.getCurrentSection(), offset, flag, true, 0);

	symbolTable.insert(n, result);
}

void Assembler::onAddDetected(std::string symbols, unsigned int line)
{
	std::regex r("(add[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(4); //2 & 4 are operands
		src = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;

	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	//13 1101
	//0110 1000
	int res = 0x68 | (oneByte ? size1B : size2B);

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (!this->checkAddressing(opDescr))
	{
		std::cout << "Dest op. cannot be immed at line" << line << std::endl;
		exit(-1);
	}

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "           " << symbols << std::endl;

	this->writeIntoSection(fin, temp);

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onPushDetected(std::string symbols, unsigned int line)
{
	std::regex r("(push[ \t\r\n]+)(.+)");
	std::smatch m;
	std::string result;
	if (std::regex_search(symbols, m, r))
	{
		result = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;
	this->locationCounter += 1;

	int data = addressing(result);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	//9 1001 1100
	//0100 1000
	int res = 0x48 | (oneByte ? size1B : size2B);
	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());
	std::cout << temp << "			" << fin << std::endl;
	//treba popraviti kada je oneByte = false dopisati nule

	this->writeIntoSection(fin, temp);
	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onPopDetected(std::string symbols, unsigned int line)
{
	std::regex r("(pop[ \t\r\n]+)(.+)");
	std::smatch m;
	std::string result;
	if (std::regex_search(symbols, m, r))
	{
		result = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = locationCounter;

	int data = addressing(result);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	//10 1010
	//0101 0000
	int res = 0x50 | (oneByte ? size1B : size2B);
	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());
	std::cout << temp << "			" << fin << "			" << symbols << std::endl;

	//treba popraviti kada je oneByte = false dopisati nule

	this->writeIntoSection(fin, this->locationCounter);
	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onXchgDetected(std::string symbols, unsigned int line)
{
	std::regex r("(xchg[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string result, dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(4); //2 & 4 are operands
		src = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;
	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	//11 1101
	//0101 1000
	int res = 0x58 | (oneByte ? size1B : size2B);

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "			" << symbols << std::endl;

	this->writeIntoSection(fin, temp);
	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onMovDetected(std::string symbols, unsigned int line)
{
	std::regex r("(mov[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string result, dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(4); //2 & 4 are operands
		src = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;

	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	//12 1100
	//0110 0000
	int res = 0x60 | (oneByte ? size1B : size2B);

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "			" << symbols << std::endl;

	this->writeIntoSection(fin, temp);

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onSubDetected(std::string symbols, unsigned int line)
{
	std::regex r("(sub[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string result, dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(4); //2 & 4 are operands
		src = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;

	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	//14 1110
	//0111 0000
	int res = 0x70 | (oneByte ? size1B : size2B);

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "           " << symbols << std::endl;

	this->writeIntoSection(fin, temp);

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onMulDetected(std::string symbols, unsigned int line)
{
	std::regex r("(mul[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string result, dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(4); //2 & 4 are operands
		src = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;

	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	//15 1111
	//0111 1000
	int res = 0x78 | (oneByte ? size1B : size2B);

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "            " << std::endl;

	this->writeIntoSection(fin, temp);

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onDivDetected(std::string symbols, unsigned int line)
{
	std::regex r("(div[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string result, dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(2); //2 & 4 are operands
		src = m.str(4);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;

	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	//16 10000
	//1000 0000
	int res = 0x80 | (oneByte ? size1B : size2B);

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "          " << std::endl;

	this->writeIntoSection(fin, temp);

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

void Assembler::onCmpDetected(std::string symbols, unsigned int line)
{
	std::regex r("(cmp[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string result, dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(4); //2 & 4 are operands
		src = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;

	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	//17 10001
	//1000 1000
	int res = 0x88 | (oneByte ? size1B : size2B);

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "          " << std::endl;

	this->writeIntoSection(fin, temp);

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}

bool Assembler::checkAddressing(std::bitset<8> bs)
{
	if (!bs[7] && !bs[6] && !bs[5])
		return false;
	else
		return true;
}

void Assembler::writeIntoSection(std::string content, unsigned long int i)
{
	Section sec(i, content, symbolTable.getCurrentSection());
	
	for(int i=0; i<sections.size(); i++)
	{
		if(sections[i].size()>0)
			if(sections[i][0].getSection() == symbolTable.getCurrentSection())
			{
				sections[i].push_back(sec);
				return;
			}
	}
	std::vector<Section> _section; _section.push_back(sec); sections.push_back(_section);
}

void Assembler::writeIntoRelRecord(RelocRecordData rrd)
{
	for(int i=0; i<relocs.size(); i++)
		if(relocs[i].size()>0)
			if(relocs[i][0].getSection() == symbolTable.getCurrentSection()) 
			{
				relocs[i].push_back(rrd);
				return;
			}

	std::vector<RelocRecordData> _rrd; _rrd.push_back(rrd); relocs.push_back(_rrd);		

}

void Assembler::checkIfDefined()
{
	int index = 1;

	for (int i = 0; i < symbolTable.umap.size(); i++)
		for (auto it = symbolTable.umap.begin(); it != symbolTable.umap.end(); ++it)
			if (it->second.serialNum == i && it->second.size > 0) //section
			{
				finalSTable.umap.insert(std::make_pair(it->first, it->second));
				finalSTable.umap.at(it->first).setSerialNum(index++);
			}

	for (int i = 0; i < symbolTable.umap.size(); i++)
		for (auto it = symbolTable.umap.begin(); it != symbolTable.umap.end(); ++it)
			if (it->second.serialNum == i && !it->second.local) //global symbols
			{
				finalSTable.umap.insert(std::make_pair(it->first, it->second));
				finalSTable.umap.at(it->first).setSerialNum(index++);
			}

	this->finalSTable.checkIfDefined();
	//this->symbolTable.checkIfDefined(); for all info
	std::ofstream myfile;
	std::cout << "*****************SYMTABLE********************" << std::endl;
	for (auto it = symbolTable.umap.begin(); it != symbolTable.umap.end(); ++it)
	{
		std::cout << it->second.serialNum << "	" << it->first << "	" << it->second.section << "	" << it->second.value << " " << it->second.size << " " << it->second.local << std::endl;
	}

	for(int i=0; i<relocs.size(); i++)
	{
		if(relocs[i].size()>0)
		{
		myfile.open(outputFile, std::ios::out | std::ios::app);
		myfile << "#rel."<<relocs[i][0].getSection() << std::endl; //#rel.text
		myfile.close();
		}
	
		for(int j=0; j<relocs[i].size(); j++)
		{
			for (auto it = symbolTable.umap.begin(); it != symbolTable.umap.end(); ++it)
				if (relocs[i][j].getSerialNo() == it->second.serialNum)
					relocs[i][j].writeToFile(finalSTable.umap.at(it->first).serialNum);		
		}
	}


}

void Assembler::backPatching(std::vector<FlinkData> addresses, int value, std::string sect)
{
	for (int j = 0; j < addresses.size(); j++)
	{
		std::vector<Section> section = getSection(addresses[j].getSection());

		int min = INT32_MAX, razlika = INT32_MAX;
		for (int i = 0; i < section.size(); i++)
		{
			if (addresses[j].getAddress() - section[i].getOffset() < razlika && addresses[j].getAddress() - section[i].getOffset() >= 0)
			{
				razlika = addresses[j].getAddress() - section[i].getOffset();
				min = i;
			}
		}

		int i = min;
		int index = addresses[j].getAddress() - section[i].getOffset();
		std::string content = section[i].getContent();
		std::stringstream stream1;
		stream1 << std::hex << value;
		std::string res1(stream1.str());
		if (res1.size() < 2)
			res1 = "0" + res1;
		if (res1.size() == 3)
			res1 = "0" + res1;
		//if(res1.size() == 4 )  res1 = res1[1] + res1[0] + res1[3] + res1[2];

		std::string temp = "0";
		if (content[index * 2] == temp[0] && content[index * 2 + 1] == temp[0] && content[index * 2 + 2] == temp[0] && content[index * 2 + 3] == temp[0])
		{
			for (int m = 0; m < res1.size(); m++)
				content[index * 2 + m] = res1[m];
			for (int m = 0; m < 4 - res1.size(); m++)
			{
				std::string temp = "0";
				content[index * 2 + res1.size() + m] = temp[0];
			}
		}
		else
		{
			int val = value - 2;
			if (addresses[j].getSection() == sect)
				val -= addresses[j].getAddress();
			std::stringstream stream2;
			stream2 << std::hex << val;
			std::string res2(stream2.str());
			if (res2.size() < 2)
				res2 = "0" + res2;
			if (res2.size() == 3)
				res2 = "0" + res2; //wrong
			res1 = res2;
			for (int m = 0; m < res1.size(); m++)
				content[index * 2 + m] = res1[m];
			for (int m = 0; m < 4 - res1.size(); m++)
			{
				std::string temp = "0";
				content[index * 2 + res1.size() + m] = temp[0];
			}
		}

		section[i].setContent(content);

		this->setContent(i, addresses[j].getSection(), content);

		if (addresses[j].getSection() == this->symbolTable.getCurrentSection())
			checkForReloc(addresses[j].getSection(), addresses[j].getAddress());
	}
}

void Assembler::setContent(int index, std::string section, std::string content)
{
	for(int i=0; i<sections.size(); i++)
		if(sections[i].size()>0)
			if(sections[i][0].getSection() == section) { sections[i][index].setContent(content); return; }

	std::cout << "NO SECTION DETECTED" << std::endl;
			

}

std::vector<Section> Assembler::getSection(std::string section)
{
	for(int i=0; i<sections.size(); i++)
		if(sections[i].size()>0)
			if(sections[i][0].getSection() == section) return sections[i];
	
	return std::vector<Section>();
}

void Assembler::addToSection(std::string section, char op)
{	
	if(doesEquExists(section))
	{
		if(op == '-')
		equSections.at(section)--;
		else equSections.at(section)++;
	} 
	else 
	{
		equSections.insert(std::make_pair(section,0));	
			if(op == '-')
		equSections.at(section)--;
		else equSections.at(section)++;
	}	
}
int Assembler::ifRelocExists(std::vector<RelocRecordData> reloc, unsigned long int lc)
{
	int index = -1;
	for (int i = 0; i < reloc.size(); i++)
	{
		if (reloc[i].getOffset() == lc)
			return i;
	}
	return index;
}

void Assembler::checkForReloc(std::string sec, unsigned long int lc)
{
	for(int i=0; i<relocs.size(); i++)
	{
		int index = ifRelocExists(relocs[i],lc);
		if(index != -1) relocs[i].erase(relocs[i].begin() + index);
	}

}

void Assembler::onJMPDetected(std::string symbols, unsigned int line)
{
	jump(symbols,line, "jmp");
}


void Assembler::onJEQDetected(std::string symbols, unsigned int line)
{
	jump(symbols,line, "jeq");
}

void Assembler::onJNEDetected(std::string symbols, unsigned int line)
{
	jump(symbols,line, "jne");
	int res = 0x3C; //velicina op. je uvek 2B
}

void Assembler::onJGTDetected(std::string symbols, unsigned int line)
{
	jump(symbols,line, "jgt");
	int res = 0x44; //velicina op. je uvek 2B
}



void Assembler::jump(std::string symbols, unsigned int line, std::string nameOfInstr)
{
	std::regex r("(.*[ \t\r\n]+)(.+)");
	std::smatch m;
	std::string result;
	if (std::regex_search(symbols, m, r))
	{
		result = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	oneByte = false;
	int temp = this->locationCounter;
	this->locationCounter += 1;

	int data = this->jumpDetected(result);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());
	if (res1.size() == 8)
		res1 = res1.substr(4, 8);

	if (res1 == "fffe")
		res1 = "feff";
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	//	if(res1.size() == 4 )  res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() == 2)
		res1 = res1 + "00";

	int res = 0x44; //velicina op. je uvek 2B

		//jmp, jne, jgt, call
	if(nameOfInstr == "jmp") res = 0x2c;	
	else if(nameOfInstr == "jgt") res = 0x44; 
	else if(nameOfInstr == "jne") res = 0x3C; 
	else if(nameOfInstr == "jeq") res = 0x34; 
	else if(nameOfInstr == "call") res = 0x24; 
	else 
	{
		std::cout<<"NIJE PRONADJEN SKOK "<<nameOfInstr<<std::endl;
		exit(-1);
	}	

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());
	std::cout << temp << "			" << fin << std::endl;

	this->writeIntoSection(fin, temp);
	this->locationCounter = this->locationCounter + 3; //one for OC two for data
}


void Assembler::basicInstr(std::string symbols, unsigned int line, std::string nameOfInstr)
{
	std::regex r("(.*[ \t\r\n]+)(.+)(,)(.+)");
	std::smatch m;
	std::string dest, src;
	if (std::regex_search(symbols, m, r))
	{
		dest = m.str(4); //2 & 4 are operands
		src = m.str(2);
	}
	else
	{
		std::cout << "No match is found" << std::endl;
		return;
	}

	int temp = this->locationCounter;

	this->locationCounter = this->locationCounter + 1;

	int data = addressing(src);
	std::stringstream stream1, stream2;
	stream1 << std::hex << data;
	std::string res1(stream1.str());

	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (res1.size() < 2)
		res1 = "0" + res1;
	if (res1.size() == 3)
		res1 = "0" + res1;
	if (res1.size() == 4)
		res1 = res1[1] + res1[0] + res1[3] + res1[2];
	if (!oneByte && res1.size() == 2)
		res1 = res1 + "00";

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;

	int res = 0;
	if(nameOfInstr == "not") res = 0x90 | (oneByte ? size1B : size2B);	
	else if(nameOfInstr == "and") res = 0x98 | (oneByte ? size1B : size2B);
	else if(nameOfInstr == "or") res = 0xA0 | (oneByte ? size1B : size2B); 
	else if(nameOfInstr == "xor") res = 0xA8 | (oneByte ? size1B : size2B); 
	else if(nameOfInstr == "test") res = 0xB0 | (oneByte ? size1B : size2B);
	else if(nameOfInstr == "shl") res = 0xB8 | (oneByte ? size1B : size2B); 
	else if(nameOfInstr == "shr") res = 0xC0 | (oneByte ? size1B : size2B); 
	else 
	{
		std::cout<<"NIJE PRONADJENA INSTRUKCIJA "<<nameOfInstr<<std::endl;
		exit(-1);
	}	

	stream2 << std::hex << res << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream2 << "0";
	std::string fin = data != -1 ? (stream2.str()) + res1 : (stream2.str());

	data = addressing(dest);
	std::stringstream stream3, stream4;
	stream3 << std::hex << data;
	std::string res2(stream3.str());

	if (!this->checkAddressing(opDescr))
	{
		std::cout << "Dest op. cannot be immed at line" << line << std::endl;
		exit(-1);
	}

	if (res2.size() < 2)
		res2 = "0" + res2;
	if (res2.size() == 3)
		res2 = "0" + res2;
	if (!oneByte && res2.size() == 2)
		res2 = res2 + "00";

	stream4 << std::hex << opDescr.to_ulong();
	if (opDescr.to_ulong() == 0)
		stream4 << "0";
	fin = fin + (data != -1 ? (stream4.str()) + res2 : (stream4.str()));

	std::cout << temp << "			" << fin << "           " << symbols << std::endl;

	this->writeIntoSection(fin, temp);

	this->locationCounter = this->locationCounter + 1;
	if (data != -1)
		this->locationCounter += oneByte ? 1 : 2;
}