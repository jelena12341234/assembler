%option c++

%{
#include <stdio.h>
#include <stdlib.h>
#include "../SS_Projekat/Assembler.h"

static Assembler assembler;

%}

%option yylineno
%option noyywrap

byte \.byte[ \t\r\n].*(,[ \t\r\n]+.*)*
word \.word[ \t\r\n].*(,[ \t\r\n]+.*)*
labela     [A-Za-z_][A-Za-z_0-9]*:
global \.global[ \t\r\n]+[A-Za-z]+(,[ \t\r\n]+[A-Za-z]+)*
extern \.extern[ \t\r\n]+[A-Za-z]+(,[ \t\r\n]+[A-Za-z]+)*
section \.section[ \t\r\n]+[A-Za-z_]+:
end \.end
skip \.skip[ \t\r\n]+[1-9][0-9]*
halt halt
iret iret
ret ret
int int[ \t\r\n]+.+
call call[ \t\r\n]+.+
jmp jmp[ \t\r\n]+.+
jeq jeq[ \t\r\n]+.+
jne jne[ \t\r\n]+.+
jgt jgt[ \t\r\n]+.+
push push[ \t\r\n]+.+
pop pop[ \t\r\n]+.+
xchg xchg[ \t\r\n]+.+,.+
mov mov[ \t\r\n]+.+,.+
add add[ \t\r\n]+.+,.+
sub sub[ \t\r\n]+.+,.+
mul mul[ \t\r\n]+.+,.+
div div[ \t\r\n]+.+,.+
cmp cmp[ \t\r\n]+.+,.+
not not[ \t\r\n]+.+,.+
and and[ \t\r\n]+.+,.+
or or[ \t\r\n]+.+,.+
xor xor[ \t\r\n]+.+,.+
test test[ \t\r\n]+.+,.+
shl shl[ \t\r\n]+.+,.+
shr shr[ \t\r\n]+.+,.+
equ \.equ[ \t\r\n]+.+,.+
%%

 /* Print delimiters. */
[(]         {printf("(left-parenthesis %u)\n", yylineno);}
[)]         {printf("(right-parenthesis %u)\n", yylineno);}
[;]         {printf("(semicolon %u)\n", yylineno);}


 /* Print identifiers, integers and operators. */
{byte}          { assembler.OnByteMatched(yytext,yylineno);}
{word}          {assembler.OnWordMatched(yytext,yylineno);}
{labela}        {assembler.onLabelMatched(yytext,yylineno);}
{global}        {assembler.onGlobalMatched(yytext,yylineno);}
{extern}        {assembler.onExternMatched(yytext,yylineno);}
{section}       {assembler.onSectionMatched(yytext,yylineno);}
{end}           {assembler.onEndMatched();}
{halt}          {assembler.onHaltDetected(yytext,yylineno);}
{ret}           {assembler.onRetDetected(yytext,yylineno);}
{iret}          {assembler.onIRetDetected(yytext,yylineno);}
{equ}           { assembler.OnEquMatched(yytext,yylineno); }
{jmp}	        { assembler.onJMPDetected(yytext,yylineno);}
{jeq}           { assembler.onJEQDetected(yytext,yylineno);}
{jne}           { assembler.onJNEDetected(yytext,yylineno);}
{jgt}           { assembler.onJGTDetected(yytext,yylineno);  }
{push}          { assembler.onPushDetected(yytext,yylineno); }
{pop}           { assembler.onPopDetected(yytext,yylineno); }
{xchg}          { assembler.onXchgDetected(yytext,yylineno);}
{mov}           { assembler.onMovDetected(yytext,yylineno); } 
{add}           { assembler.onAddDetected(yytext,yylineno); } 
{sub}           { assembler.onSubDetected(yytext,yylineno);}
{mul}           { assembler.onMulDetected(yytext,yylineno);}
{div}           { assembler.onDivDetected(yytext,yylineno);}
{cmp}           { assembler.onCmpDetected(yytext,yylineno);}
{not}           { assembler.onNotDetected(yytext,yylineno);}
{and}           { assembler.onAndDetected(yytext,yylineno);}
{or}            { assembler.onOrDetected(yytext,yylineno);}
{xor}           { assembler.onXorDetected(yytext,yylineno);}
{test}          { assembler.onTestDetected(yytext,yylineno);}
{shl}           { assembler.onShlDetected(yytext,yylineno);}
{shr}           { assembler.onShrDetected(yytext,yylineno);} 
{skip} 	        { assembler.onSkipMatched(yytext); }


#[^\n]*     {}
[ \t\r\n]   {}

. 	{ printf("Error near '%s' at line %d!\n", yytext, yylineno); return -1; }

<<EOF>>     {printf("(eof %u)\n", yylineno); return 0;}

%%