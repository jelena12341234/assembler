.global a, c
.extern b
.section text:
	jmp *a(%r7)
	jmp *e(%r7)
	jmp *b(%r7)
	jmp *d(%r7)
d: .word d
	mov %r1,b
	mov c,%r1
	mov %r1,e
.section data:
.skip 8
e: .byte 12
   .word c
a: .word b
   .word 16
.section bss:
 c: .skip 8
.end