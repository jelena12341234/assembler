.global loop, labjedan, kraj
.extern nizPocetak, nizKraj
.section text:
mov $0,%r1  
mov $nizPocetak,%r3
loop:  
cmp *(%r3),%r1
jgt labjedan
add $2,%r3
cmp $nizKraj,%r3
jgt loop
jmp kraj
labjedan:
mov *(%r3),%r1
add $2,%r3
cmp $nizKraj,%r3
jgt loop
kraj:
halt
.end