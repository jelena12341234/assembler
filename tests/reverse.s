.global loop
.extern nizPocetak, nizKraj
.section text:
mov $nizPocetak,%r1  
mov $nizKraj,%r2  
sub $2,%r2
loop:
mov *(%r1),%r3
mov *(%r2),*(%r1)
mov %r3,*(%r2)
add $2,%r1
sub $2,%r2
cmp %r2,%r1
jgt loop
halt
.end